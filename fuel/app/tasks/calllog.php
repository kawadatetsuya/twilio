<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Fuel\Tasks;

/**
 * Robot example task
 *
 * Ruthlessly stolen from the beareded Canadian sexy symbol:
 *
 *		Derek Allard: http://derekallard.com/
 *
 * @package		Fuel
 * @version		1.0
 * @author		Phil Sturgeon
 */

class Calllog
{
	// Twilio Account Infomation
	// (stdClass Object)
	// string sid: Twilio Account Sid
	// string token: Twilio Application Token
	protected static $accounts = null;
	// Dates
	// (stdClass Object)
	// string $today: today date
	// string $tomorrow: tomorrow date
	protected static $dates = null;
	
	// selector
	protected static $selector = null;
	
	// twilio properties 
	// key: twilio property name
	// value: sales force column name
	protected static $properties = array(
//			"sid",
//			"parent_call_sid",
//			"date_created",
//			"date_updated",
//			"account_sid",
			"to" => "To__c",					// Phone to
			"from" => "From__c",					// Call From
//			"phone_number_sid",
			"status" => "Status__c",				// user status
			"start_time" => "StartTime__c",			// Start time
			"end_time" => "EndTime__c",	
			"duration" => "Duration__c",
//			"price",
//			"direction",
//			"answered_by",
//			"forwarded_from",
//			"caller_name",
//			"uri",
	);
	
	// records of twilio log
	protected static $results = array();
	
	// salesforce object
	protected static $sforce = null;
	protected static $sforceLogin = null;

	// help message
	protected static $commands = "";	

	/**
	 * @param $selector string			// log account
	 * @param String $today				// today date(format: Y-m-d, default: today)
	 * @return string
	 */
	public static function run($selector = false, $today = false)
	{
		try{
			self::init();
			self::main($selector, $today);
		}catch( \Exception $e ){
			$message = "Error: ". $e->getMessage();
			\Log::error( $e->getMessage() );
		
			echo $message."\n";
			echo "Usage: php oil r calllog ".self::$commands." [Y-m-d]\n\n";
			return;
		}
	}

	/**
	 * @param $selector string			// log account
	 * @param String $today				// today date(format: Y-m-d, default: today)
	 */
	protected static function main($selector, $today){
		self::getTwilioAccount( $selector );
		self::getStartDate($today);
		self::getTwilioLog();
		self::putSalesforce();
	}
	
	/*
	 *	initialize
	 */
	protected static function init(){
		// require PEAR::Services_Twilio
		require_once('Services'.DS.'Twilio.php');
		// require salesForce Client
		$salesforcePath = APPPATH.'vendor'.DS.'Force.com-Toolkit-for-PHP'.DS.'soapclient'.DS;
		require_once($salesforcePath.'SforceEnterpriseClient.php');
		// load configuration
		\Config::load('app');
		
		// create salesforce object
		$username = \Config::get('app.salesforce.username');
		$token = \Config::get('app.salesforce.password').\Config::get('app.salesforce.token');
		$sforce = new \SforceEnterpriseClient();
		$sforce->createConnection($salesforcePath.'enterprise.wsdl.xml');
		self::$sforceLogin = $sforce->login($username, $token);	// if invalid user access then throw exception 
		self::$sforce = $sforce;
		
		// make help message
		$accounts = \Config::get('app.api.account');
		$commands = "";
		$count = 0;
		foreach( $accounts as $command => $value ){
			$commands .= ( ( $count !== 0 )?"|":""). $command;
			$count++;
		}
		self::$commands = $commands;
	}

	
	/*
	 * put data to salesforce DB
	 */
	protected static function putSalesforce(){
		$results = self::$results;
		if( sizeof( $results ) > 0 )
			$response = self::$sforce->create($results, \Config::get('app.salesforce.objectName.'. self::$selector));
	}
	/*
	 * get data for twilio access log
	 *	@return $results array()	// alldatas
	 */
	protected static function getTwilioLog(){
	// make header
//		$results[] = self::$properties;
		$results = array();
	// make body
		$twilio = new \Services_Twilio(self::$accounts->sid, self::$accounts->token);
		foreach ($twilio->account->calls->getIterator(0, 50, array(
			'StartTime>' => self::$dates->today, 
			'StartTime<' => self::$dates->tomorrow,
		) ) as $call) {
			$datas = new \stdClass;
			foreach( self::$properties as $key => $value ){
				switch( $key ){
					case "date_created":
					case "date_updated":
					case "start_time":
					case "end_time":
						$datas->$value = date('Y-m-d H:i:s', strtotime( $call->$key ) );
						break;
					default:
						$datas->$value = $call->$key;
						break;
				}
			
			}

			// Filter: direction property
			switch( $call->direction ){
				case "outbound-dial":
				// Filter: status property
					switch( $call->status ){
						case "completed":
						case "failed":
						case "busy":
						case "no-answer":
							$results[] = $datas;
							break;
						case "queued":
						case "ringing":
						case "in-progress":
						default:
							break;
					}
					break;
				case "outbound-api":
				case "inbound":
				default:
					break;
			}
		}
		
		return self::$results = $results;
	}
	
	/*
	 *
	 */
	protected static function putCSV(){
		// output csv
			\Config::load('file');
			$fileName = date('Ymd', strtotime( self::$dates->today )).".csv";
			$file = \Config::get('basedir'). self::$accounts->selector. DS. $fileName;
			
			$resource = \File::open_file(@fopen($file, 'w'), true );
			if( $resource === false )
				throw new \Exception("Couldn't create file: ". $file);
			
			foreach( $results as $data ){
				fputcsv( $resource, $data );
			}
			\File::close_file( $resource );
	}
	
	
	/*
	 * get Twilio account parameters
	 *	@param $selector string			// log account
	 *  @return stdClass 				// string sid: Twilio Account Sid, string token: Twilio Application Token
	 *  @exception \Exception			// unknown command
	 */
	protected static function getTwilioAccount($selector = false){
		$return = new \stdClass;
		\Config::load('app');

		try{
			if( $selector === false )
				throw new \Exception("Command Not Found");
			if( \Config::get('app.api.account.'. $selector) === null )
				throw new \Exception("Invalid command: ". $selector);
		}
		catch( \Exception $e ){
			throw $e;
		}
		
		$return->sid = \Config::get('app.api.account.'. $selector. '.sid');
		$return->token = \Config::get('app.api.account.'. $selector. '.token');
		
		self::$selector = $selector;
		return self::$accounts = $return;
	}
	/*
	 * create date for twilio StartTime parameter
	 *	@param $today string			// start date (command line second parameter)
	 *  @return stdClass 				// string $today: today date, string $tomorrow: tomorrow date
	 *  @exception \Exception		    // invalid date
	 */
	protected static function getStartDate($today = false){
		$return = new \stdClass;
		
		$tomorrow = "";
		if( $today !== false ){
			if( strtotime( $today ) !== false ){
				$today = date('Y-m-d', strtotime($today));
			}
			else{
				throw new \Exception('Invalid date: '. $today);
			}
		}
		else{
			$today = date('Y-m-d', strtotime('yesterday'));
		}
		$tomorrow = date('Y-m-d', strtotime($today.' +1 day'));
		
		$return->today = $today;
		$return->tomorrow = $tomorrow;

		return self::$dates = $return;
	}	
}

/* End of file tasks/robots.php */
