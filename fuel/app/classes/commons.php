<?php
/*
 *	common methods
 */

class Commons{
	/*
	 *	validation
	 * @param $json string	// JSON Raw data
	 * @param $exception bool  // true: if data was not valid then throw exception
	 * @return	bool		// is valid is true
	 */
	public static function isValid( $json, $exception = false ){
		\Log::debug('start Common::isValid');
		$parsed = json_decode( $json );
		$message = false;
		$status = '000';

		if( isset( $parsed->status ) ){
			$status = $parsed->status;
			switch( $parsed->status ){
				case '200':
					return true;
					break;
				case '400':
					$message = 'API Request Parameter Wrong: '. $json;
					break;
				case '403':
					$message = 'API Access Denied: '. $json;
					break;
				case '500':
					$message = 'API Internal Server Error: '. $json;
					break;
				case '503':
					$message = 'API Salesforce Access Denied: '. $json;
					break;
				default:
					break;
			}
		}
		else
			$message = "Unknown Status: ". $json;
		
		if( $message ){
			\Log::info('Throw Exception: '. $json);
			throw new Exception($message, $status);
		}
		else{
			$message = 'Invalid Parameter: '. $json;
			\Log::warning($message);
		}
		
		if( $exception )
			throw new Exception($message, $status);
		else
			return false;
				
	}

	/*
	 *	get NWF(CMS) data
	 * 
	 * @param $kindOfApi	string		// kind of api  ["search", "estimate"]  reference: app/config/app.php
	 * @param $queries 	array			// query strings
	 * @param $method   string			// kind of form method ["GET", "POST"]
	 * @return $json	string			// json string
	 * @exception Exception				// API Access Error
	 */
	public static function getNWFJson( $kindOfApi = 'search', $queries = array(), $method = "GET" ){
//		$requests =	http_build_query( $queries, '', '&' );
		$ch = curl_init();
		
		switch( $method ){
			case "POST":
			case "post":
				curl_setopt($ch, CURLOPT_POST, $isPost);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $queries);
		//		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);		
				$uri = self::makeApiUri($kindOfApi);
				break;
			default:
				$uri = self::makeApiUri($kindOfApi, $queries);
				break;
		}

		\Log::info($method.": ".$uri);

		curl_setopt($ch, CURLOPT_URL, $uri);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, \Config::get('app.api.username') . ":" . \Config::get('app.api.password') );
		
		$json = curl_exec($ch);
		$infos = curl_getinfo ($ch);
		curl_close($ch);

		if( $infos['http_code'] !== 200 ){
//			var_dump( $infos );
			$message = 'Api Access denied: '. $uri;
			\Log::error($message);
			throw new Exception($message);
		}
		
		return $json;
	}
	
	/*
	 *	make request uri
	 * @param $kindOfApi	string		// kind of api  ["search", "estimate"]  reference: app/config/app.php
	 * @param $queries 	array			// query strings
	 * @param $uri
	 */
	public static function makeApiUri( $kindOfApi = 'search', $queries = array() ){
		\Config::load('app');
		$return = null;
	// make api
		$base = \Config::get('app.api.baseUrl');
		$path = \Config::get('app.api.path.'. $kindOfApi);
		if( !$path ){
			$path = \Config::get('app.api.path.search');
		}
/*
		$queries = array(
			"store_t_number" => "05011111111",
		);
*/		
		$uri = Uri::create($base. $path, array(), $queries);
		
		return $uri;
	}


 
 
 
 
	public static function test(){
		echo "test";
		\Config::load('app');
		print_r( $api = \Config::get('app.api') );
		var_dump( \Config::get('app.api.baseUrl') );
	}
}
 