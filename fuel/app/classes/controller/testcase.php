<?php

class Controller_Testcase extends Controller
{
	public function before()
	{
		parent::before();
		if( Fuel::$env !== \Fuel::DEVELOPMENT )
			return Response::forge(ViewModel::forge('welcome/404'), 404);
	}
	
	public function after($response)
	{
		return parent::after($response);
	}
	
	protected function common($controller){
		$view = 'testcase/'. $controller.'.html';
		$view = View::forge($view);
		$view->title = 'Testcase &raquo; '. $controller;
		$view->set("commentIn", "<!--", false);
		$view->set("commentOut", "-->", false);
		return $view;
	}

	public function action_search()
	{
		$controller = 'search';
		$view = self::common($controller);
		$view->actionUri = \Uri::create('concierge/'. $controller.'/index.xml');
		return $view;
	}

	public function action_estimate01()
	{
		$controller = "estimate";
		$view = self::common('estimate01');
		$view->actionUri = \Uri::create('concierge/'. $controller.'/index.xml', array(), $_GET );
		
		$count = \Input::get("count");
		$selenium = <<<SELENIUM
	count: ${count}
SELENIUM;
		
		$view->selenium = $selenium;
		return $view;
	}
	
	public function action_estimate02()
	{
		$controller = "estimate";
		$view = self::common('estimate02');
		$view->actionUri = \Uri::create('concierge/'. $controller.'/item.xml', array(), $_GET );

		$count = \Input::get("count");
		$storeId = \Input::get("storeId");
		$selenium = <<<SELENIUM
	count: ${count}
	storeId: ${storeId}
SELENIUM;
		
		$view->selenium = $selenium;
		return $view;
	}

	public function action_estimate03()
	{
		$controller = "estimate";
		$storeId = \Input::get("storeId","0001");		
		$view = self::common('estimate03');
		$view->actionUri = \Uri::create('concierge/'. $controller.'/validate.xml', array(), $_GET );
		
		$count = \Input::get("count");
		$storeId = \Input::get("storeId");
		$selenium = <<<SELENIUM
	count: ${count}
	storeId: ${storeId}
SELENIUM;
		
		$view->selenium = $selenium;
		return $view;
	}
	
	public function action_estimate04()	
	{
		$controller = "estimate";
		$storeId = \Input::get("storeId","0001");		
		$view = self::common('estimate03');
		$view->actionUri = \Uri::create('https://cc83dem-ark-app000.c4sa.net/concierge/'. $controller.'/validate.xml', array(), $_GET );
		
		$count = \Input::get("count");
		$storeId = \Input::get("storeId");
		$selenium = <<<SELENIUM
	count: ${count}
	storeId: ${storeId}
SELENIUM;
		
		$view->selenium = $selenium;
		return $view;
	}
	
}
