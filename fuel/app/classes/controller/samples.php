<?php

class Controller_Samples extends Controller
{
	public function action_index()
	{
		return Response::forge(ViewModel::forge('welcome/404'), 404);
	}

	public function action_inbound()
	{
//		$this->template->title = 'Samples &raquo; Inbound';
//		$this->template->content = View::forge('samples/inbound');
		return Response::forge(ViewModel::forge('samples/inbound'));
	}

	public function action_outbound()
	{
//		$this->template->title = 'Samples &raquo; Outbound';
//		$this->template->content = View::forge('samples/outbound');
		return Response::forge(ViewModel::forge('samples/outbound'));
	}

	public function action_makeacall()
	{
//		$this->template->title = 'Samples &raquo; Inbound';
//		$this->template->content = View::forge('samples/inbound');
		return Response::forge(ViewModel::forge('samples/makeacall.xml'));
	}
	
	public function action_accounts(){
//		require_once('Services'.DS.'Twilio.php');
		include_once(APPPATH.'vendor'.DS.'Services'.DS.'Twilio.php');
		
		$properties = array(
			"sid",
			"parent_call_sid",
			"date_created",
			"date_updated",
			"account_sid",
			"to",
			"from",
			"phone_number_sid",
			"status",
			"start_time",
			"end_time",
			"duration",
			"price",
			"direction",
			"answered_by",
			"forwarded_from",
			"caller_name",
			"uri",
		);

		foreach( $properties as $property ){
			echo $property.",";
		}
		echo "\n";

		
		$twilio = new Services_Twilio('AC3aed9d056d1160d5b0a1617cc9d4a020','0aab49c601fb746b453e24875dc67db8');
		foreach ($twilio->account->calls->getIterator(0, 50, array('StartTime>' => '2013-03-13') ) as $call) {
			foreach( $properties as $property ){
				echo $call->$property."\t";
			}
			echo "\n\n";
		}
	
		return Response::forge(View::forge('samples/accounts'));
	}
	
	public function action_twiml(){
		$view = 'samples/hellomonkey.xml';
		
		\Log::debug(print_r( $_REQUEST, true ));
		$response = Response::forge(ViewModel::forge($view));
		$response->set_header('Content-Type', 'application/xml');
		return $response;
	}
	
	public function action_twimlhandle(){
		$view = 'samples/hellomonkeyhandle.xml';
		
		$response = Response::forge(ViewModel::forge($view));
		$response->set_header('Content-Type', 'application/xml');
		return $response;
	}

	public function action_twimlrecode(){
		$view = 'samples/hellomonkeyrecode.xml';
		
		$response = Response::forge(ViewModel::forge($view));
		$response->set_header('Content-Type', 'application/xml');
		return $response;
	}
	
}
