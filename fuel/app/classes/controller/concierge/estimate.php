<?php

class Controller_Concierge_Estimate extends Controller
{
	public function before(){
		\Asset::instance()->add_type('mp3');
		\Asset::add_path('assets/');
		\Asset::add_path('assets/estimate/');
		parent::before();
	}

	public function after($response){
		$response->set_header('Content-Type', 'application/xml');
		return parent::after($response);
	}

	public function action_index()
	{
		$view = ViewModel::forge('concierge/estimate/index.xml');
		return Response::forge( $view );
	}

	public function action_item()
	{
		$view = ViewModel::forge('concierge/estimate/item.xml');
		return Response::forge( $view );
	}

	public function action_validate()
	{
	// get parameter
		$count = (int)\Input::get('count', 0);
		$storeId = \Input::get('storeId', '0000');		
		$queries = array(
			"store_id" => $storeId,
			"anken_id" => \Input::post('Digits'),
		);
		
	// create default view
		$view = ViewModel::forge('concierge/estimate/redirect.xml');
		
		try{
			$result = \Commons::getNWFJson('estimate', $queries);
/*	
// invalid item
			$result = <<<RESULT
{
	"status": "430"
}
RESULT;

*/
/*
// expired item
			$result = <<<RESULT
{
	"status": "200",
	"user_r_number": "09033333333",
	"store_t_number": "05000000000",
	"expires": "20130301150000"
}
RESULT;
*/			

/*
// success item
			$result = <<<RESULT
{
	"status": "200",
	"user_r_number": "08010484117",
	"store_t_number": "05031331511",
	"expires": "20130531090000"
}
RESULT;


*/			
		// validation
			if( \Commons::isValid($result) ){
				$parsed = json_decode( $result );
			// date check
				$today = date('YmdHis');
				$expires = $parsed->expires;
				if( $today < $expires ){
				// success 
					$view = ViewModel::forge('concierge/estimate/complete.xml');
					$view->forwardToNumber = $parsed->user_r_number;
					return Response::forge( $view );
				}
			// date is not valid
				\Log::warning("This item was expired. Expired Date: ". $expires);
				$view = ViewModel::forge('concierge/estimate/expired.xml');	
				return Response::forge( $view );
			}
		// system error
		}catch( Exception $error){
			\Log::error('System Error: '. $error->getMessage() );
			$view = ViewModel::forge('concierge/estimate/error.xml');	
			return Response::forge( $view );
		}
	
	// count check
		if( $count > 1 ){
			\Log::warning("Count Over");
			$view = ViewModel::forge('concierge/estimate/countover.xml');
			return Response::forge( $view );
		}
		else{
			$count++;
			$view->count = $count;
		}

	// redirect page
//		self::addComment( $view, $count, $storeId );
		return Response::forge( $view );
	}
	
/*
	public function action_complete()
	{
		return Response::forge(ViewModel::forge('concierge/estimate/complete.xml'));
	}

	public function action_error()
	{
		return Response::forge(ViewModel::forge('concierge/estimate/error.xml'));
	}

	public function action_redirect()
	{
		return Response::forge(ViewModel::forge('concierge/estimate/redirect.xml'));
	}
*/
}
