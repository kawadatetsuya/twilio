<?php

class Controller_Concierge_Search extends Controller
{
	private static $_debug = false;
	
	public function before(){
		\Asset::instance()->add_type('mp3');
		\Asset::add_path('assets/');
		\Asset::add_path('assets/search/');
//		echo \Asset::get_file('common_error_01.mp3', 'mp3');
		parent::before();
	}

	public function after($response){
		$response->set_header('Content-Type', 'application/xml');
		return parent::after($response);
	}

	public function action_index()
	{
	// get parameters
		$queries = array(
			"store_t_number" => \Input::post('CalledVia'),
		);
		
		try{
			$path = 'concierge/search/index.xml';

			if( !self::$_debug ){
				$result = \Commons::getNWFJson('search', $queries);
			// validation
				\Commons::isValid($result, true);
			// set index view
				$parsed = json_decode( $result );
			}
			else{
				$parsed = new stdClass();
				$parsed->store_r_number = "08012345678";
			}

			$view = ViewModel::forge($path);
			$view->forwardToNumber = $parsed->store_r_number;
		}
		catch( Exception $error){
			$path = 'concierge/search/error.xml';
			$view = ViewModel::forge($path);			
		}
		
		return Response::forge($view);
	}
	
	
/*
	public function action_error()
	{
		return Response::forge(ViewModel::forge('concierge/search/error.xml'));
	}
*/
}
