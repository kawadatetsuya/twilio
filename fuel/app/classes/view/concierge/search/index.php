<?php

class View_Concierge_Search_index extends Viewmodel
{
	public function view()
	{
	// change phone number format
		$number = $this->forwardToNumber;
		$regex = <<<REGEX
!0([0-9]+)!i
REGEX;
		$replace = <<<REPLACE
+81$1
REPLACE;
		$number = preg_replace( $regex, $replace, $number );
		$this->forwardToNumber = $number;
		
	// set mp3 urls
		$this->voiceInfo01 = \Asset::get_file('search_info_01.mp3', 'mp3');
		$this->voiceCommonWarn01 = \Asset::get_file('common_warn_01.mp3', 'mp3');
	}
}