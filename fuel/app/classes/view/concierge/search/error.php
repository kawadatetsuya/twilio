<?php

class View_Concierge_Search_error extends Viewmodel
{
	public function view()
	{
		$this->content = "Concierge_Search &raquo; error";

		$this->voiceCommonError01 = \Asset::get_file('common_error_01.mp3', 'mp3');
	}
}