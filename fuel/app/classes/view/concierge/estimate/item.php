<?php

class View_Concierge_Estimate_item extends Viewmodel
{
	public function view()
	{
	// get parameter
		$count = \Input::get('count', 0);
		$storeId = \Input::post('Digits', '0000');		

		$queries = array(
			'count'=>$count,
			"storeId"=>$storeId,
		);
		$this->actionUri = \Uri::create('concierge/estimate/validate.xml', array(), $queries);
		$this->numDigits = 10;
		
		$this->voiceInfo03 = \Asset::get_file('estimate_info_03.mp3', 'mp3');
		$this->voiceWarn01 = \Asset::get_file('estimate_warn_01.mp3', 'mp3');
	}
}