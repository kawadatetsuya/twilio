<?php

class View_Concierge_Estimate_index extends Viewmodel
{
	public function view()
	{
	// get parameter
		$count = \Input::get('count', 0);
		$queries = array(
			'count'=>$count,
		);
		$this->actionUri = \Uri::create('concierge/estimate/item.xml', array(), $queries);
		$this->numDigits = 4;
		
		$this->voiceInfo01 = \Asset::get_file('estimate_info_01.mp3', 'mp3');
		$this->voiceInfo02 = \Asset::get_file('estimate_info_02.mp3', 'mp3');
		$this->voiceWarn01 = \Asset::get_file('estimate_warn_01.mp3', 'mp3');
	}
}