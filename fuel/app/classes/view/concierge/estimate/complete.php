<?php

class View_Concierge_Estimate_complete extends Viewmodel
{
	public function view()
	{
		$number = $this->forwardToNumber;
		$regex = <<<REGEX
!0([0-9]+)!i
REGEX;
		$replace = <<<REPLACE
+81$1
REPLACE;
		$number = preg_replace( $regex, $replace, $number );
		$this->forwardToNumber = $number;
		
		$this->voiceInfo04 = \Asset::get_file('estimate_info_04.mp3', 'mp3');
		$this->voiceCommonWarn01 = \Asset::get_file('common_warn_01.mp3', 'mp3');
	}
}