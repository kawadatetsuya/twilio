<?php

class View_Concierge_Estimate_redirect extends Viewmodel
{
	public function view()
	{
		$queries = array(
			'count'=>$this->count,
		) ;
		$this->redirectUrl = \Uri::create('concierge/estimate/index.xml', array(), $queries);

		$this->voiceWarn02 = \Asset::get_file('estimate_warn_02.mp3', 'mp3');
	}
}