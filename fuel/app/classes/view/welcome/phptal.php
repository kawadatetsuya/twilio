<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Welcome_Phptal extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	public function view()
	{
		// let's create an array of objects for test purpose
		$people = array();
		$people[] = new Person("foo", "01-344-121-021");
		$people[] = new Person("bar", "05-999-165-541");
		$people[] = new Person("baz", "01-389-321-024");
		$people[] = new Person("quz", "05-321-378-654");
	
		$this->title = 'The title value';
		$this->people = $people;
	}
}

// the Person class
class Person {
	public $name;
	public $phone;

	function Person($name, $phone) {
		$this->name = $name;
		$this->phone = $phone;
	}
}
