<?php

/**
 * The which view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Samples_Hellomonkey extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 * 
	 * @return void
	 */
	public function view()
	{
		\Log::Info('start "View_Samples_Hellomonkey" view model');
		$this->name = "Kawada Monkey Test";
		$this->set('attrAction', \Uri::create('samples/twimlhandle.xml'), false );
	}
}

