<?php

/**
 * The which view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Samples_Inbound extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 * 
	 * @return void
	 */
	public function view()
	{
		\Log::Info('start "View_Samples_Inbound" view model');
		echo "view model";
	}
	
}

