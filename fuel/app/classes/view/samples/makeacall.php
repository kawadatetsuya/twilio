<?php

/**
 * The which view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Samples_Makeacall extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 * 
	 * @return void
	 */
	public function view()
	{
		\Log::Info('start "View_Samples_Makeacall" view model');


		$call = Twilio\Twilio::request('MakeCall');
		$response = $call->create(array(
			'To' => '+81-80-1048-4117',
			'From' => '+815031331511',
			'Url' => 'http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient',
		));

/*		
		// Install the library via PEAR or download the .zip file to your project folder.
		// This line loads the library
		require('Services/Twilio.php');

		$sid = "AC3aed9d056d1160d5b0a1617cc9d4a020"; // Your Account SID from www.twilio.com/user/account
		$token = "0aab49c601fb746b453e24875dc67db8"; // Your Auth Token from www.twilio.com/user/account

		$client = new Services_Twilio($sid, $token);
//		$client->_http->debug = true;
		$call = $client->account->calls->create(
		  '+815031331511', // From a valid Twilio number
		  '+81-80-1048-4117', // Call this number

		  // Read TwiML at this URL when a call connects (hold music)
		  'http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient'
		);
*/		
		$this->title = "make a call";
//		print_r( $response );
//		exit();
		$this->sid = $response->sid;
		$this->phoneFrom = $response->from;
		$this->phoneTo = $response->to;
	}
}

