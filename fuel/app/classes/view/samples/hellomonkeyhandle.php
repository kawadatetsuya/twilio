<?php

/**
 * The which view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Samples_Hellomonkeyhandle extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 * 
	 * @return void
	 */
	public function view()
	{
		\Log::Info('start "View_Samples_Hellomonkeyhandle" view model');
		$key = \Input::param('Digits');
		\Log::debug('Digits:'.$key);
		
		$this->set('actionRecode', Uri::create('samples/twimlrecode.xml'), false);
		
		$this->forwardMode = false;
		$this->recodeMode = false;
		
		switch( $key ){
			case 1:
				$this->forwardMode = true;
				break;
			case 2:
				$this->recodeMode = true;
				break;
			default:
				\Response::redirect('samples/twiml.xml');
				break;
		}
		
	}
}

