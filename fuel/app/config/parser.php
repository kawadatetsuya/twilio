<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.0
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2012 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(

	// ------------------------------------------------------------------------
	// Register extensions to their parsers, either classname or array config
	// ------------------------------------------------------------------------
	'extensions' => array(
		'html'    => 'View_Phptal',
		'xml'    => 'View_Phptal',
	),

	// Phptal ( http://phptal.org/manual/en/ )
	// ------------------------------------------------------------------------
	'View_Phptal'   => array(
		'include'   => APPPATH.'vendor'.DS.'PHPTAL'.DS.'PHPTAL.php',
	),
);

// end of file parser.php
